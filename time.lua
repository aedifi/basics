-- time.lua
-- Implements /time and time-related subcommands.

local PlayerTimeCommandUsage = "Usage: /time <day | night | value> [world]"
local ConsoleTimeCommandUsage = "Usage: time set < day | night | value> [world]"

-- Translate our arbitrary labels to in-game values.
local SpecialTimesTable = {
	["day"] = 1000,
	["night"] = 1000 + 12000,
}

local TimeAnimationInProgress = false

-- Animates the transition between old and new time values.
local function SetTime(World, TimeToSet)
	local CurrentTime = World:GetTimeOfDay()
    local MaxTime = 24000
    
	-- Handle the cases where TimeToSet < 0 or > 24000.
	TimeToSet = TimeToSet % MaxTime
	local AnimationSpeed = 480
	if CurrentTime > TimeToSet then
		AnimationSpeed = -AnimationSpeed
    end
    
	local function DoAnimation()
		if not TimeAnimationInProgress then
			return
		end
		local TimeOfDay = World:GetTimeOfDay()
		local AnimatedTime = TimeOfDay + AnimationSpeed
		local Animate = (
			((AnimationSpeed > 0) and (AnimatedTime < TimeToSet))
			or ((AnimationSpeed < 0) and (AnimatedTime > TimeToSet))
		)
		if Animate then
			World:SetTimeOfDay(AnimatedTime)
			World:ScheduleTask(1, DoAnimation)
		else
			World:SetTimeOfDay(TimeToSet)
			TimeAnimationInProgress = false
		end
    end
    
	if TimeAnimationInProgress then
		TimeAnimationInProgress = false
		World:SetTimeOfDay(TimeToSet)
	else
		TimeAnimationInProgress = true
		World:ScheduleTask(1, DoAnimation)
    end
	return true
end

local function CommonSpecialTime(World, TimeName)
	-- Make sure the world is valid...
	if not World then
		return true
	end
	local TimeToSet = World:GetTimeOfDay() + TimeToAdd
	SetTime(World, SpecialTimesTable[TimeName])
	return true
end

-- /time <day | night> [world]
function HandleSpecialTimeCommand(Split, Player)
	return CommonSpecialTime(GetWorld(Split[3], Player), Split[2])
end

function HandleConsoleSpecialTime(a_Split)
	return CommonSpecialTime(GetWorld( a_Split[3] ), a_Split[2])
end
